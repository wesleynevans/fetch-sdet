#!/usr/bin/python3

import random
import time

from Model.Page import Page

class WeightProgram:
	def __init__(self):
		self.__Page = Page();
		
	def Run(self):
		try:
			possibleAnswers = list(map(lambda possibleAnswer: possibleAnswer.Text(), self.__Page.Answers().AnswerButtons()));
			weighingCount = 0;
			while len(possibleAnswers) > 1:
				possibleAnswers = self.__EliminateWrongAnswers(possibleAnswers);
				weighingCount += 1;
			answer = possibleAnswers[0];
			print("Fake Bar:", answer);
			for possibleAnswer in self.__Page.Answers().AnswerButtons():
				if possibleAnswer.Text() == answer:
					possibleAnswer.Click();
					break;
			print(self.__Page.PopupAlert().Text());
			self.__Page.PopupAlert().Accept();
			print("Weighings Count:", weighingCount);
			print("Weighings:");
			for weighing in self.__Page.Weighings().List():
				print("          ", weighing.Text());
		except Exception as ex:
			print(ex);
		finally:
			self.__Page.Quit();
		
	def __EliminateWrongAnswers(self, possibleAnswers):
		self.__Reset();

		random.shuffle(possibleAnswers);		
		sideCount = int((len(possibleAnswers)+1)/3);
		leftBowl = possibleAnswers[0: sideCount];
		rightBowl = possibleAnswers[sideCount: 2*sideCount];
		notInBowl = possibleAnswers[2*sideCount: ];

		self.__SetBowl(leftBowl, self.__Page.LeftBowl());
		self.__SetBowl(rightBowl, self.__Page.RightBowl());
		self.__Weigh();

		resultText = self.__Page.Result().Text();
		if "<" == resultText:
			return leftBowl;
		if ">" == resultText:
			return rightBowl;
		return notInBowl;
		
	def __Reset(self):
		if self.__Page.Result().Text() == "?":
			return;
		self.__Page.ActionButtons().Reset().Click();
		for count in range(10):
			if self.__Page.Result().Text() == "?":
				return;
			time.sleep(.5);
		
	def __Weigh(self):
		if self.__Page.Result().Text() != "?":
			return;
		self.__Page.ActionButtons().Weigh().Click();
		for count in range(10):
			if self.__Page.Result().Text() != "?":
				return;
			time.sleep(.5);
		
	def __SetBowl(self, values, bowl):
		inputs = bowl.Inputs();
		random.shuffle(inputs);
		for index in range(len(values)):
			inputs[index].SetText(values[index]);
		
weightProgram = WeightProgram();
weightProgram.Run();
