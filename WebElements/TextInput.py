#!/bin/python3

class TextInput:
	def __init__(self, webElement):
		self.__WebElement = webElement;
			
	def GetText(self):
		return self.__WebElement.text();
		
	def SetText(self, text):
		self.__WebElement.send_keys(text);
		
