#!/usr/bin/python3

from selenium.webdriver.common.alert import Alert

class PopupAlert:
	def __init__(self, driver):
		self.__Driver = driver;
			
	def Accept(self):
		Alert(self.__Driver).accept();
		
	def Text(self):
		return Alert(self.__Driver).text;
		
