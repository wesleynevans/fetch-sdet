#!/bin/python3

class Button:
	def __init__(self, webElement):
		self.__WebElement = webElement;
			
	def Text(self):
		return self.__WebElement.text;
		
	def Click(self):
		self.__WebElement.click();
