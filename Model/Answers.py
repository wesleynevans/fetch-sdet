#!/usr/bin/python3

from WebElements.Button import Button

class Answers:
	def __init__(self, driver):
		self.__Driver = driver;
		self.__CssSelectors = [];
		for index in range(9):
			cssSelectors = "button[id=coin_%d]" % index;
			self.__CssSelectors.append(cssSelectors);
		
	def AnswerButtons(self):
		results = map(lambda cssSelectors: self.__BuildElement(cssSelectors), self.__CssSelectors);
		return list(results);
		
	def __BuildElement(self, cssSelectors):
		element = self.__Driver.find_element_by_css_selector(cssSelectors);
		return Button(element);
