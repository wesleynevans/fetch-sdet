#!/usr/bin/python3

from WebElements.TextElement import TextElement

class Weighings:
	def __init__(self, driver):
		self.__Driver = driver;
		self.__CssSelector = "div[class=game-info] ol li";
		
	def List(self):
		elements = self.__Driver.find_elements_by_css_selector(self.__CssSelector);
		results = map(lambda element: TextElement(element), elements);
		return list(results);
		
