#!/usr/bin/python3

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from Model.ActionButtons import ActionButtons
from Model.Answers import Answers
from Model.Bowl import Bowl
from Model.Weighings import Weighings

from WebElements.PopupAlert import PopupAlert
from WebElements.TextElement import TextElement

class Page:
	def __init__(self):
		self.__Driver = webdriver.Firefox();
		self.__Driver.implicitly_wait(10);
		self.__Driver.get("http://ec2-54-208-152-154.compute-1.amazonaws.com/");		
		wait = WebDriverWait(self.__Driver, 10);
		wait.until(EC.title_is("React App"));

		self.__ResultCss = "button[id=reset]:disabled";
		
	def LeftBowl(self):
		return Bowl(self.__Driver, "left");
	
	def Result(self):
		element = self.__Driver.find_element_by_css_selector(self.__ResultCss);
		return TextElement(element);
			
	def RightBowl(self):
		return Bowl(self.__Driver, "right");
	
	def ActionButtons(self):
		return ActionButtons(self.__Driver);
			
	def Answers(self):
		return Answers(self.__Driver);
		
	def Weighings(self):
		return Weighings(self.__Driver);
		
	def PopupAlert(self):
		return PopupAlert(self.__Driver);
		
	def Quit(self):
		self.__Driver.close();
		
