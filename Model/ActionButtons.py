#!/usr/bin/python3

from WebElements.Button import Button

class ActionButtons:
	def __init__(self, driver):
		self.__Driver = driver;
		self.__ResetCssSelector = "button[id=reset]:not([disabled])";
		self.__WeighCssSelector = "button[id=weigh]";
		
	def Reset(self):
		element = self.__Driver.find_element_by_css_selector(self.__ResetCssSelector);
		return Button(element);
		
	def Weigh(self):
		element = self.__Driver.find_element_by_css_selector(self.__WeighCssSelector);
		return Button(element);
