#!/usr/bin/python3

from WebElements.TextInput import TextInput

class Bowl:
	def __init__(self, driver, side):
		self.__Driver = driver;
		self.__CssSelectors = [];
		for index in range(9):
			cssSelector = "input[id=%s_%d]" % (side, index);
			self.__CssSelectors.append(cssSelector);
		
	def Inputs(self):
		results = map(lambda cssSelector: self.__BuildElement(cssSelector), self.__CssSelectors);
		return list(results);
		
	def __BuildElement(self, cssSelector):
		element = self.__Driver.find_element_by_css_selector(cssSelector);
		return TextInput(element);
