This is a submission for the Fetch application exercise https://fetch-hiring.s3.amazonaws.com/SDET/FetchRewards_Coding_Exercise_SDET.pdf

We will assume the user is running Ubuntu 20.04 with sudo privileges. 

# Setup
Before the first run, we need to do some setup.

Run the following commands in the command prompt in the directory this README.md file is located:
- `sudo apt install python3 python3-pip firefox=92.0+build3-0ubuntu0.20.04.1`
- `pip3 install -r Requirements.txt`
- `chmod +x WeightProgram.py`
- `cp geckodriver ~/.local/bin`

# Run
To run this program, run the following command in the command prompt in the directory this README.md file is located:

./WeightProgram.py
